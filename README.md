# TinyRexx to Cpp
This is an assignment for a job interview.

## Related works
It takes inspiration from two projects I have done in the past:
- [TinyRexxGMT](https://github.com/jjocram/tinyRexxGMT): from this it takes the idea. It is a transpiler from TinyRexx (a simpler version of [the Rexx programming language](https://en.wikipedia.org/wiki/Rexx)) to C++. With respect to this project I changed programming language (from C++ to Python) and I improved the grammar to avoid some casts.
- [SimpLanPlus](https://github.com/jjocram/SimpLanPlus): from this it takes the knowledge. SimpLanPlus is a language designed for the Compilers and Interpreters course of the Master's Degree in Computer Science A.Y. 2020/21. For this project the grammar of the language has been described and techniques and methodologies have been implemented to perform semantic analysis, with particular attention to effect analysis and type checking. An intermediate SVM-Assembly language has been defined, which is taken as input by an interpreter also designed and implemented within this project.

![pipeline](https://gitlab.com/jjocram/tinyrexxtocpp/badges/master/pipeline.svg)
![coverage](https://gitlab.com/jjocram/tinyrexxtocpp/badges/master/coverage.svg)
## Examples
`examples` directory contains 4 examples of tinyRexx programs converted to C++ using this tool

## Installation
You can install this tool with pip (or pipx)
```bash
pip install git+https://gitlab.com/jjocram/tinyrexxtocpp.git    
```

## Usage
If you installed it as a package
```bash
tinyrexxtocpp PATH_TINYREXX_SOURCE_FILE PATH_CPP_DESTINATIOn_FILE
```

Or inside `src` directory after installing the dependencies with `pip install -r requirements.txt`
```bash
python -m tinyrexxtocpp PATH_TINYREXX_SOURCE_FILE PATH_CPP_DESTINATIOn_FILE
```

## Technologies and tools
- Python3
- Antlr4 and its runtime for Python3
- PyTest
- VIM and PyCharm (as IDEs)
- Git
- GitLab CI/CD