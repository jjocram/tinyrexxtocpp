# Generated from TinyRexx.g4 by ANTLR 4.10.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,32,166,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,
        2,14,7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,1,0,4,0,40,8,0,
        11,0,12,0,41,1,0,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,53,8,1,1,2,
        1,2,1,2,1,2,1,3,1,3,1,3,1,3,1,4,1,4,1,4,1,5,1,5,1,5,1,6,1,6,1,6,
        1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,3,6,86,8,
        6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,5,6,96,8,6,10,6,12,6,99,9,6,1,
        7,1,7,1,8,1,8,1,8,1,8,1,8,1,8,1,9,1,9,1,9,1,9,1,9,1,9,1,9,1,10,1,
        10,1,10,1,10,1,10,1,10,1,10,3,10,123,8,10,1,11,1,11,1,11,1,11,1,
        11,1,12,4,12,131,8,12,11,12,12,12,132,1,13,1,13,1,13,1,13,1,13,1,
        13,1,13,1,13,1,13,3,13,144,8,13,1,13,1,13,1,13,1,13,5,13,150,8,13,
        10,13,12,13,153,9,13,1,14,1,14,1,15,1,15,1,16,1,16,1,17,1,17,1,18,
        1,18,1,18,1,18,0,2,12,26,19,0,2,4,6,8,10,12,14,16,18,20,22,24,26,
        28,30,32,34,36,0,3,1,0,14,18,1,0,19,23,1,0,24,25,165,0,39,1,0,0,
        0,2,52,1,0,0,0,4,54,1,0,0,0,6,58,1,0,0,0,8,62,1,0,0,0,10,65,1,0,
        0,0,12,85,1,0,0,0,14,100,1,0,0,0,16,102,1,0,0,0,18,108,1,0,0,0,20,
        115,1,0,0,0,22,124,1,0,0,0,24,130,1,0,0,0,26,143,1,0,0,0,28,154,
        1,0,0,0,30,156,1,0,0,0,32,158,1,0,0,0,34,160,1,0,0,0,36,162,1,0,
        0,0,38,40,3,2,1,0,39,38,1,0,0,0,40,41,1,0,0,0,41,39,1,0,0,0,41,42,
        1,0,0,0,42,43,1,0,0,0,43,44,5,0,0,1,44,1,1,0,0,0,45,53,3,4,2,0,46,
        53,3,8,4,0,47,53,3,10,5,0,48,53,3,16,8,0,49,53,3,36,18,0,50,53,3,
        20,10,0,51,53,3,18,9,0,52,45,1,0,0,0,52,46,1,0,0,0,52,47,1,0,0,0,
        52,48,1,0,0,0,52,49,1,0,0,0,52,50,1,0,0,0,52,51,1,0,0,0,53,3,1,0,
        0,0,54,55,5,27,0,0,55,56,5,1,0,0,56,57,3,26,13,0,57,5,1,0,0,0,58,
        59,5,27,0,0,59,60,5,1,0,0,60,61,3,26,13,0,61,7,1,0,0,0,62,63,5,2,
        0,0,63,64,3,26,13,0,64,9,1,0,0,0,65,66,5,3,0,0,66,67,5,27,0,0,67,
        11,1,0,0,0,68,69,6,6,-1,0,69,70,3,26,13,0,70,71,3,32,16,0,71,72,
        3,26,13,0,72,86,1,0,0,0,73,74,5,4,0,0,74,75,3,12,6,0,75,76,5,5,0,
        0,76,86,1,0,0,0,77,86,3,26,13,0,78,79,5,26,0,0,79,86,3,26,13,0,80,
        81,5,26,0,0,81,82,5,4,0,0,82,83,3,12,6,0,83,84,5,5,0,0,84,86,1,0,
        0,0,85,68,1,0,0,0,85,73,1,0,0,0,85,77,1,0,0,0,85,78,1,0,0,0,85,80,
        1,0,0,0,86,97,1,0,0,0,87,88,10,5,0,0,88,89,3,34,17,0,89,90,3,12,
        6,6,90,96,1,0,0,0,91,92,10,1,0,0,92,93,3,32,16,0,93,94,3,12,6,2,
        94,96,1,0,0,0,95,87,1,0,0,0,95,91,1,0,0,0,96,99,1,0,0,0,97,95,1,
        0,0,0,97,98,1,0,0,0,98,13,1,0,0,0,99,97,1,0,0,0,100,101,3,12,6,0,
        101,15,1,0,0,0,102,103,5,6,0,0,103,104,5,7,0,0,104,105,3,12,6,0,
        105,106,3,24,12,0,106,107,5,8,0,0,107,17,1,0,0,0,108,109,5,6,0,0,
        109,110,3,6,3,0,110,111,5,9,0,0,111,112,3,28,14,0,112,113,3,24,12,
        0,113,114,5,8,0,0,114,19,1,0,0,0,115,116,5,10,0,0,116,117,3,14,7,
        0,117,118,5,11,0,0,118,119,5,6,0,0,119,120,3,24,12,0,120,122,5,8,
        0,0,121,123,3,22,11,0,122,121,1,0,0,0,122,123,1,0,0,0,123,21,1,0,
        0,0,124,125,5,12,0,0,125,126,5,6,0,0,126,127,3,24,12,0,127,128,5,
        8,0,0,128,23,1,0,0,0,129,131,3,2,1,0,130,129,1,0,0,0,131,132,1,0,
        0,0,132,130,1,0,0,0,132,133,1,0,0,0,133,25,1,0,0,0,134,135,6,13,
        -1,0,135,144,5,27,0,0,136,144,5,28,0,0,137,138,5,4,0,0,138,139,3,
        26,13,0,139,140,5,5,0,0,140,144,1,0,0,0,141,142,5,14,0,0,142,144,
        3,26,13,1,143,134,1,0,0,0,143,136,1,0,0,0,143,137,1,0,0,0,143,141,
        1,0,0,0,144,151,1,0,0,0,145,146,10,2,0,0,146,147,3,30,15,0,147,148,
        3,26,13,3,148,150,1,0,0,0,149,145,1,0,0,0,150,153,1,0,0,0,151,149,
        1,0,0,0,151,152,1,0,0,0,152,27,1,0,0,0,153,151,1,0,0,0,154,155,3,
        26,13,0,155,29,1,0,0,0,156,157,7,0,0,0,157,31,1,0,0,0,158,159,7,
        1,0,0,159,33,1,0,0,0,160,161,7,2,0,0,161,35,1,0,0,0,162,163,5,13,
        0,0,163,164,3,26,13,0,164,37,1,0,0,0,9,41,52,85,95,97,122,132,143,
        151
    ]

class TinyRexxParser ( Parser ):

    grammarFileName = "TinyRexx.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'='", "'say'", "'pull'", "'('", "')'", 
                     "'do'", "'while'", "'end'", "'to'", "'if'", "'then'", 
                     "'else'", "'exit'", "'-'", "'+'", "'*'", "'/'", "'%'", 
                     "'=='", "'<'", "'<='", "'>'", "'>='", "'&'", "'|'", 
                     "'\\'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "MINUS", "PLUS", "MUL", 
                      "DIV", "MOD", "EQUAL", "LT", "LEQ", "GT", "GEQ", "AND", 
                      "OR", "NOT", "ID", "NUMBER", "WS", "LINECOMMENTS", 
                      "BLOCKCOMMENTS", "ERR" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_assign = 2
    RULE_loop_assign = 3
    RULE_say = 4
    RULE_pull = 5
    RULE_test = 6
    RULE_if_test = 7
    RULE_w_loop = 8
    RULE_do_loop = 9
    RULE_if_st = 10
    RULE_else_st = 11
    RULE_body = 12
    RULE_a_expr = 13
    RULE_loop_a_expr = 14
    RULE_a_op = 15
    RULE_r_op = 16
    RULE_b_op = 17
    RULE_terminate = 18

    ruleNames =  [ "program", "statement", "assign", "loop_assign", "say", 
                   "pull", "test", "if_test", "w_loop", "do_loop", "if_st", 
                   "else_st", "body", "a_expr", "loop_a_expr", "a_op", "r_op", 
                   "b_op", "terminate" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    MINUS=14
    PLUS=15
    MUL=16
    DIV=17
    MOD=18
    EQUAL=19
    LT=20
    LEQ=21
    GT=22
    GEQ=23
    AND=24
    OR=25
    NOT=26
    ID=27
    NUMBER=28
    WS=29
    LINECOMMENTS=30
    BLOCKCOMMENTS=31
    ERR=32

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.10.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(TinyRexxParser.EOF, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TinyRexxParser.StatementContext)
            else:
                return self.getTypedRuleContext(TinyRexxParser.StatementContext,i)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)




    def program(self):

        localctx = TinyRexxParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 39 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 38
                self.statement()
                self.state = 41 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyRexxParser.T__1) | (1 << TinyRexxParser.T__2) | (1 << TinyRexxParser.T__5) | (1 << TinyRexxParser.T__9) | (1 << TinyRexxParser.T__12) | (1 << TinyRexxParser.ID))) != 0)):
                    break

            self.state = 43
            self.match(TinyRexxParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def assign(self):
            return self.getTypedRuleContext(TinyRexxParser.AssignContext,0)


        def say(self):
            return self.getTypedRuleContext(TinyRexxParser.SayContext,0)


        def pull(self):
            return self.getTypedRuleContext(TinyRexxParser.PullContext,0)


        def w_loop(self):
            return self.getTypedRuleContext(TinyRexxParser.W_loopContext,0)


        def terminate(self):
            return self.getTypedRuleContext(TinyRexxParser.TerminateContext,0)


        def if_st(self):
            return self.getTypedRuleContext(TinyRexxParser.If_stContext,0)


        def do_loop(self):
            return self.getTypedRuleContext(TinyRexxParser.Do_loopContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = TinyRexxParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.state = 52
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 45
                self.assign()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 46
                self.say()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 47
                self.pull()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 48
                self.w_loop()
                pass

            elif la_ == 5:
                self.enterOuterAlt(localctx, 5)
                self.state = 49
                self.terminate()
                pass

            elif la_ == 6:
                self.enterOuterAlt(localctx, 6)
                self.state = 50
                self.if_st()
                pass

            elif la_ == 7:
                self.enterOuterAlt(localctx, 7)
                self.state = 51
                self.do_loop()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AssignContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TinyRexxParser.ID, 0)

        def a_expr(self):
            return self.getTypedRuleContext(TinyRexxParser.A_exprContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_assign

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssign" ):
                listener.enterAssign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssign" ):
                listener.exitAssign(self)




    def assign(self):

        localctx = TinyRexxParser.AssignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 54
            self.match(TinyRexxParser.ID)
            self.state = 55
            self.match(TinyRexxParser.T__0)
            self.state = 56
            self.a_expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Loop_assignContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TinyRexxParser.ID, 0)

        def a_expr(self):
            return self.getTypedRuleContext(TinyRexxParser.A_exprContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_loop_assign

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLoop_assign" ):
                listener.enterLoop_assign(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLoop_assign" ):
                listener.exitLoop_assign(self)




    def loop_assign(self):

        localctx = TinyRexxParser.Loop_assignContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_loop_assign)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            self.match(TinyRexxParser.ID)
            self.state = 59
            self.match(TinyRexxParser.T__0)
            self.state = 60
            self.a_expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SayContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def a_expr(self):
            return self.getTypedRuleContext(TinyRexxParser.A_exprContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_say

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSay" ):
                listener.enterSay(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSay" ):
                listener.exitSay(self)




    def say(self):

        localctx = TinyRexxParser.SayContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_say)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self.match(TinyRexxParser.T__1)
            self.state = 63
            self.a_expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PullContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TinyRexxParser.ID, 0)

        def getRuleIndex(self):
            return TinyRexxParser.RULE_pull

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPull" ):
                listener.enterPull(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPull" ):
                listener.exitPull(self)




    def pull(self):

        localctx = TinyRexxParser.PullContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_pull)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 65
            self.match(TinyRexxParser.T__2)
            self.state = 66
            self.match(TinyRexxParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TestContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def a_expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TinyRexxParser.A_exprContext)
            else:
                return self.getTypedRuleContext(TinyRexxParser.A_exprContext,i)


        def r_op(self):
            return self.getTypedRuleContext(TinyRexxParser.R_opContext,0)


        def test(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TinyRexxParser.TestContext)
            else:
                return self.getTypedRuleContext(TinyRexxParser.TestContext,i)


        def NOT(self):
            return self.getToken(TinyRexxParser.NOT, 0)

        def b_op(self):
            return self.getTypedRuleContext(TinyRexxParser.B_opContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_test

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTest" ):
                listener.enterTest(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTest" ):
                listener.exitTest(self)



    def test(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyRexxParser.TestContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 12
        self.enterRecursionRule(localctx, 12, self.RULE_test, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 69
                self.a_expr(0)
                self.state = 70
                self.r_op()
                self.state = 71
                self.a_expr(0)
                pass

            elif la_ == 2:
                self.state = 73
                self.match(TinyRexxParser.T__3)
                self.state = 74
                self.test(0)
                self.state = 75
                self.match(TinyRexxParser.T__4)
                pass

            elif la_ == 3:
                self.state = 77
                self.a_expr(0)
                pass

            elif la_ == 4:
                self.state = 78
                self.match(TinyRexxParser.NOT)
                self.state = 79
                self.a_expr(0)
                pass

            elif la_ == 5:
                self.state = 80
                self.match(TinyRexxParser.NOT)
                self.state = 81
                self.match(TinyRexxParser.T__3)
                self.state = 82
                self.test(0)
                self.state = 83
                self.match(TinyRexxParser.T__4)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 97
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,4,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 95
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
                    if la_ == 1:
                        localctx = TinyRexxParser.TestContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_test)
                        self.state = 87
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 88
                        self.b_op()
                        self.state = 89
                        self.test(6)
                        pass

                    elif la_ == 2:
                        localctx = TinyRexxParser.TestContext(self, _parentctx, _parentState)
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_test)
                        self.state = 91
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 92
                        self.r_op()
                        self.state = 93
                        self.test(2)
                        pass

             
                self.state = 99
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class If_testContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def test(self):
            return self.getTypedRuleContext(TinyRexxParser.TestContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_if_test

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_test" ):
                listener.enterIf_test(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_test" ):
                listener.exitIf_test(self)




    def if_test(self):

        localctx = TinyRexxParser.If_testContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_if_test)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 100
            self.test(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class W_loopContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def test(self):
            return self.getTypedRuleContext(TinyRexxParser.TestContext,0)


        def body(self):
            return self.getTypedRuleContext(TinyRexxParser.BodyContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_w_loop

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterW_loop" ):
                listener.enterW_loop(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitW_loop" ):
                listener.exitW_loop(self)




    def w_loop(self):

        localctx = TinyRexxParser.W_loopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_w_loop)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 102
            self.match(TinyRexxParser.T__5)
            self.state = 103
            self.match(TinyRexxParser.T__6)
            self.state = 104
            self.test(0)
            self.state = 105
            self.body()
            self.state = 106
            self.match(TinyRexxParser.T__7)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Do_loopContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def loop_assign(self):
            return self.getTypedRuleContext(TinyRexxParser.Loop_assignContext,0)


        def loop_a_expr(self):
            return self.getTypedRuleContext(TinyRexxParser.Loop_a_exprContext,0)


        def body(self):
            return self.getTypedRuleContext(TinyRexxParser.BodyContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_do_loop

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDo_loop" ):
                listener.enterDo_loop(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDo_loop" ):
                listener.exitDo_loop(self)




    def do_loop(self):

        localctx = TinyRexxParser.Do_loopContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_do_loop)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 108
            self.match(TinyRexxParser.T__5)
            self.state = 109
            self.loop_assign()
            self.state = 110
            self.match(TinyRexxParser.T__8)
            self.state = 111
            self.loop_a_expr()
            self.state = 112
            self.body()
            self.state = 113
            self.match(TinyRexxParser.T__7)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class If_stContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.condition = None # If_testContext
            self.thenBranch = None # BodyContext

        def if_test(self):
            return self.getTypedRuleContext(TinyRexxParser.If_testContext,0)


        def body(self):
            return self.getTypedRuleContext(TinyRexxParser.BodyContext,0)


        def else_st(self):
            return self.getTypedRuleContext(TinyRexxParser.Else_stContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_if_st

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIf_st" ):
                listener.enterIf_st(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIf_st" ):
                listener.exitIf_st(self)




    def if_st(self):

        localctx = TinyRexxParser.If_stContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_if_st)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 115
            self.match(TinyRexxParser.T__9)
            self.state = 116
            localctx.condition = self.if_test()
            self.state = 117
            self.match(TinyRexxParser.T__10)
            self.state = 118
            self.match(TinyRexxParser.T__5)
            self.state = 119
            localctx.thenBranch = self.body()
            self.state = 120
            self.match(TinyRexxParser.T__7)
            self.state = 122
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TinyRexxParser.T__11:
                self.state = 121
                self.else_st()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Else_stContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.elseBranch = None # BodyContext

        def body(self):
            return self.getTypedRuleContext(TinyRexxParser.BodyContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_else_st

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterElse_st" ):
                listener.enterElse_st(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitElse_st" ):
                listener.exitElse_st(self)




    def else_st(self):

        localctx = TinyRexxParser.Else_stContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_else_st)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 124
            self.match(TinyRexxParser.T__11)
            self.state = 125
            self.match(TinyRexxParser.T__5)
            self.state = 126
            localctx.elseBranch = self.body()
            self.state = 127
            self.match(TinyRexxParser.T__7)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BodyContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TinyRexxParser.StatementContext)
            else:
                return self.getTypedRuleContext(TinyRexxParser.StatementContext,i)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_body

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBody" ):
                listener.enterBody(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBody" ):
                listener.exitBody(self)




    def body(self):

        localctx = TinyRexxParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 129
                self.statement()
                self.state = 132 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyRexxParser.T__1) | (1 << TinyRexxParser.T__2) | (1 << TinyRexxParser.T__5) | (1 << TinyRexxParser.T__9) | (1 << TinyRexxParser.T__12) | (1 << TinyRexxParser.ID))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class A_exprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TinyRexxParser.ID, 0)

        def NUMBER(self):
            return self.getToken(TinyRexxParser.NUMBER, 0)

        def a_expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TinyRexxParser.A_exprContext)
            else:
                return self.getTypedRuleContext(TinyRexxParser.A_exprContext,i)


        def MINUS(self):
            return self.getToken(TinyRexxParser.MINUS, 0)

        def a_op(self):
            return self.getTypedRuleContext(TinyRexxParser.A_opContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_a_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterA_expr" ):
                listener.enterA_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitA_expr" ):
                listener.exitA_expr(self)



    def a_expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyRexxParser.A_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 26
        self.enterRecursionRule(localctx, 26, self.RULE_a_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 143
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TinyRexxParser.ID]:
                self.state = 135
                self.match(TinyRexxParser.ID)
                pass
            elif token in [TinyRexxParser.NUMBER]:
                self.state = 136
                self.match(TinyRexxParser.NUMBER)
                pass
            elif token in [TinyRexxParser.T__3]:
                self.state = 137
                self.match(TinyRexxParser.T__3)
                self.state = 138
                self.a_expr(0)
                self.state = 139
                self.match(TinyRexxParser.T__4)
                pass
            elif token in [TinyRexxParser.MINUS]:
                self.state = 141
                self.match(TinyRexxParser.MINUS)
                self.state = 142
                self.a_expr(1)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 151
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = TinyRexxParser.A_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_a_expr)
                    self.state = 145
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 146
                    self.a_op()
                    self.state = 147
                    self.a_expr(3) 
                self.state = 153
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Loop_a_exprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def a_expr(self):
            return self.getTypedRuleContext(TinyRexxParser.A_exprContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_loop_a_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLoop_a_expr" ):
                listener.enterLoop_a_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLoop_a_expr" ):
                listener.exitLoop_a_expr(self)




    def loop_a_expr(self):

        localctx = TinyRexxParser.Loop_a_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_loop_a_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 154
            self.a_expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class A_opContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def MINUS(self):
            return self.getToken(TinyRexxParser.MINUS, 0)

        def PLUS(self):
            return self.getToken(TinyRexxParser.PLUS, 0)

        def MUL(self):
            return self.getToken(TinyRexxParser.MUL, 0)

        def DIV(self):
            return self.getToken(TinyRexxParser.DIV, 0)

        def MOD(self):
            return self.getToken(TinyRexxParser.MOD, 0)

        def getRuleIndex(self):
            return TinyRexxParser.RULE_a_op

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterA_op" ):
                listener.enterA_op(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitA_op" ):
                listener.exitA_op(self)




    def a_op(self):

        localctx = TinyRexxParser.A_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_a_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 156
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyRexxParser.MINUS) | (1 << TinyRexxParser.PLUS) | (1 << TinyRexxParser.MUL) | (1 << TinyRexxParser.DIV) | (1 << TinyRexxParser.MOD))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class R_opContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EQUAL(self):
            return self.getToken(TinyRexxParser.EQUAL, 0)

        def LT(self):
            return self.getToken(TinyRexxParser.LT, 0)

        def LEQ(self):
            return self.getToken(TinyRexxParser.LEQ, 0)

        def GT(self):
            return self.getToken(TinyRexxParser.GT, 0)

        def GEQ(self):
            return self.getToken(TinyRexxParser.GEQ, 0)

        def getRuleIndex(self):
            return TinyRexxParser.RULE_r_op

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterR_op" ):
                listener.enterR_op(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitR_op" ):
                listener.exitR_op(self)




    def r_op(self):

        localctx = TinyRexxParser.R_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_r_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 158
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyRexxParser.EQUAL) | (1 << TinyRexxParser.LT) | (1 << TinyRexxParser.LEQ) | (1 << TinyRexxParser.GT) | (1 << TinyRexxParser.GEQ))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class B_opContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def AND(self):
            return self.getToken(TinyRexxParser.AND, 0)

        def OR(self):
            return self.getToken(TinyRexxParser.OR, 0)

        def getRuleIndex(self):
            return TinyRexxParser.RULE_b_op

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterB_op" ):
                listener.enterB_op(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitB_op" ):
                listener.exitB_op(self)




    def b_op(self):

        localctx = TinyRexxParser.B_opContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_b_op)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 160
            _la = self._input.LA(1)
            if not(_la==TinyRexxParser.AND or _la==TinyRexxParser.OR):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TerminateContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def a_expr(self):
            return self.getTypedRuleContext(TinyRexxParser.A_exprContext,0)


        def getRuleIndex(self):
            return TinyRexxParser.RULE_terminate

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTerminate" ):
                listener.enterTerminate(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTerminate" ):
                listener.exitTerminate(self)




    def terminate(self):

        localctx = TinyRexxParser.TerminateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_terminate)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 162
            self.match(TinyRexxParser.T__12)
            self.state = 163
            self.a_expr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[6] = self.test_sempred
        self._predicates[13] = self.a_expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def test_sempred(self, localctx:TestContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def a_expr_sempred(self, localctx:A_exprContext, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         




