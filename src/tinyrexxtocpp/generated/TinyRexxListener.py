# Generated from TinyRexx.g4 by ANTLR 4.10.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .TinyRexxParser import TinyRexxParser
else:
    from TinyRexxParser import TinyRexxParser

# This class defines a complete listener for a parse tree produced by TinyRexxParser.
class TinyRexxListener(ParseTreeListener):

    # Enter a parse tree produced by TinyRexxParser#program.
    def enterProgram(self, ctx:TinyRexxParser.ProgramContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#program.
    def exitProgram(self, ctx:TinyRexxParser.ProgramContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#statement.
    def enterStatement(self, ctx:TinyRexxParser.StatementContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#statement.
    def exitStatement(self, ctx:TinyRexxParser.StatementContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#assign.
    def enterAssign(self, ctx:TinyRexxParser.AssignContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#assign.
    def exitAssign(self, ctx:TinyRexxParser.AssignContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#loop_assign.
    def enterLoop_assign(self, ctx:TinyRexxParser.Loop_assignContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#loop_assign.
    def exitLoop_assign(self, ctx:TinyRexxParser.Loop_assignContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#say.
    def enterSay(self, ctx:TinyRexxParser.SayContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#say.
    def exitSay(self, ctx:TinyRexxParser.SayContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#pull.
    def enterPull(self, ctx:TinyRexxParser.PullContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#pull.
    def exitPull(self, ctx:TinyRexxParser.PullContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#test.
    def enterTest(self, ctx:TinyRexxParser.TestContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#test.
    def exitTest(self, ctx:TinyRexxParser.TestContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#if_test.
    def enterIf_test(self, ctx:TinyRexxParser.If_testContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#if_test.
    def exitIf_test(self, ctx:TinyRexxParser.If_testContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#w_loop.
    def enterW_loop(self, ctx:TinyRexxParser.W_loopContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#w_loop.
    def exitW_loop(self, ctx:TinyRexxParser.W_loopContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#do_loop.
    def enterDo_loop(self, ctx:TinyRexxParser.Do_loopContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#do_loop.
    def exitDo_loop(self, ctx:TinyRexxParser.Do_loopContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#if_st.
    def enterIf_st(self, ctx:TinyRexxParser.If_stContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#if_st.
    def exitIf_st(self, ctx:TinyRexxParser.If_stContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#else_st.
    def enterElse_st(self, ctx:TinyRexxParser.Else_stContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#else_st.
    def exitElse_st(self, ctx:TinyRexxParser.Else_stContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#body.
    def enterBody(self, ctx:TinyRexxParser.BodyContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#body.
    def exitBody(self, ctx:TinyRexxParser.BodyContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#a_expr.
    def enterA_expr(self, ctx:TinyRexxParser.A_exprContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#a_expr.
    def exitA_expr(self, ctx:TinyRexxParser.A_exprContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#loop_a_expr.
    def enterLoop_a_expr(self, ctx:TinyRexxParser.Loop_a_exprContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#loop_a_expr.
    def exitLoop_a_expr(self, ctx:TinyRexxParser.Loop_a_exprContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#a_op.
    def enterA_op(self, ctx:TinyRexxParser.A_opContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#a_op.
    def exitA_op(self, ctx:TinyRexxParser.A_opContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#r_op.
    def enterR_op(self, ctx:TinyRexxParser.R_opContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#r_op.
    def exitR_op(self, ctx:TinyRexxParser.R_opContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#b_op.
    def enterB_op(self, ctx:TinyRexxParser.B_opContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#b_op.
    def exitB_op(self, ctx:TinyRexxParser.B_opContext):
        pass


    # Enter a parse tree produced by TinyRexxParser#terminate.
    def enterTerminate(self, ctx:TinyRexxParser.TerminateContext):
        pass

    # Exit a parse tree produced by TinyRexxParser#terminate.
    def exitTerminate(self, ctx:TinyRexxParser.TerminateContext):
        pass



del TinyRexxParser