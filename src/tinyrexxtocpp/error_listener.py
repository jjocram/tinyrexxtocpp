from antlr4.error.ErrorListener import ErrorListener

from .generated.TinyRexxParser import TinyRexxParser

class TinyRexxErrorListener(ErrorListener):
    def __init__(self, errors):
        self.errors = errors

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        error = {}

        if isinstance(recognizer, TinyRexxParser):
            error['stack'] = recognizer.getRuleInvocationStack()

        error['msg'] = f"{msg} at {line}:{column}"

        if offendingSymbol:
            error['symbol'] = offendingSymbol.text

        if e:
            error['exception'] = type(e).__name__

        self.errors.append(error)