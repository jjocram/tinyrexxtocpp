from sys import argv

from antlr4 import FileStream, CommonTokenStream, ParseTreeWalker

from .error_listener import TinyRexxErrorListener
from .generated.TinyRexxLexer import TinyRexxLexer
from .generated.TinyRexxParser import TinyRexxParser
from .listener import Listener


def main():
    if len(argv) <= 2:
        print("Missing argument")
        print("usage: tinyrexxtocpp TINIREXX_SOURCE_FILE CPP_DESTINATION_FILE")
        exit(1)

    file = FileStream(argv[1])
    lexer = TinyRexxLexer(file)
    token_stream = CommonTokenStream(lexer)
    parser = TinyRexxParser(token_stream)

    errors = []
    tinyrexx_error_listener = TinyRexxErrorListener(errors)

    parser.removeErrorListeners()
    parser.addErrorListener(tinyrexx_error_listener)

    lexer.removeErrorListeners()
    lexer.addErrorListener(tinyrexx_error_listener)

    tree = parser.program()

    if errors:
        print(f"{len(errors)} errors found")
        for error in errors:
            print("========")
            if 'stack' in error.keys():
                print(f"Rules stack: {error['stack']}")
            if 'symbol' in error.keys():
                print(f"Symbol: {error['symbol']}")
            if 'exception' in error.keys():
                print(f"Exception: {error['exception']}")
            print(f"Error: {error['msg']}")

        exit(1)

    token_stream.fill()
    ids = {token.text for token in token_stream.tokens if token.type == parser.ID}

    with open(argv[2], "w+") as output_file:
        program = Listener(ids, output_file)
        walker = ParseTreeWalker()
        walker.walk(program, tree)
