grammar TinyRexx;

options {
    language = Python3;
}

program   : statement+ EOF;
          
statement : assign 
          | say 
          | pull 
          | w_loop 
          | terminate 
          | if_st 
          | do_loop;

assign    : ID '=' a_expr ;

loop_assign : ID '=' a_expr ;

say     : 'say' a_expr ;

pull     : 'pull' ID ;

test	  : a_expr r_op a_expr 
          | '(' test ')' 
          | test b_op test 
          | a_expr 
          | NOT a_expr 
          | NOT '(' test ')' 
          | test r_op test;

if_test: test ;

w_loop    : 'do' 'while' test body 'end' ;

do_loop	  : 'do' loop_assign 'to'  loop_a_expr body 'end';

if_st	  : 'if' condition=if_test 'then' 'do' thenBranch=body 'end' (else_st)?;

else_st   : 'else' 'do' elseBranch=body 'end' ;

body	  : statement+;

a_expr    : ID 
          | NUMBER 
          | '(' a_expr ')' 
          | a_expr a_op a_expr 
          | MINUS a_expr ;

loop_a_expr : a_expr ;

a_op      : MINUS 
          | PLUS 
          | MUL 
          | DIV 
          | MOD;

r_op      : EQUAL 
          | LT 
          | LEQ 
          | GT 
          | GEQ ;

b_op	  : AND 
          | OR;

terminate : 'exit' a_expr;

// Lexer input

// Operators
MINUS     : '-' ;
PLUS      : '+' ;
MUL       : '*' ;
DIV       : '/' ;
MOD	      : '%';
EQUAL     : '==' ;
LT        : '<' ;
LEQ       : '<=' ;
GT        : '>' ;
GEQ       : '>=' ;
AND	      : '&';
OR	      : '|';
NOT	      : '\\';

// IDs
fragment CHAR 	    : 'a'..'z' |'A'..'Z' ;
ID                  : CHAR (CHAR | DIGIT)* ;

// Numbers
fragment DIGIT	    : '0'..'9';
NUMBER              : DIGIT+;

// Escape sequences
WS              : (' '|'\t'|'\n'|'\r')-> skip;
LINECOMMENTS 	: '//' (~('\n'|'\r'))* -> skip;
BLOCKCOMMENTS   : '/*'( ~('/'|'*')|'/'~'*'|'*'~'/'|BLOCKCOMMENTS)* '*/' -> skip;

ERR : . -> channel(HIDDEN);
